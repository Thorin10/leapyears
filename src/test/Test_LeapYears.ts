import 'mocha';
import { expect } from 'chai';
import { LeapYearsDivision } from '../main/LeapYears';

describe("All tests for It's a Leap Year", () => {
  it("should print 'It's not a Leap Year' if it's not divisible by 4", () => {
    let result = LeapYearsDivision(23);
    expect(result).to.be.false;
  });
  it("should print 'It's  not a Leap Year' if it's divisible by 100 but not by 400 ", () => {
    let result = LeapYearsDivision(1700);
    expect(result).to.be.false;
  });
  it("should print 'It's a Leap Year' if it's divisible by 4 but not by 100", () => {
    let result = LeapYearsDivision(2008);
    expect(result).to.be.true;
  });
  it("should print 'It's a Leap Year' if it's divisible by 400 ", () => {
    let result = LeapYearsDivision(2000);
    expect(result).to.be.true;
  });
});
