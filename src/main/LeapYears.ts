export const LeapYearsDivision = (oneInteger: number) => {
  return !(oneInteger % 400) || (oneInteger % 100 && !(oneInteger % 4))
    ? true
    : (!(oneInteger % 100) && oneInteger % 400) || oneInteger % 4
    ? false
    : false;
};
